#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

constexpr double EPS = 1e-6;

double newton(double prev, double (*f)(double), double (*f_d)(double)) {
  double cur = prev - f(prev) / f_d(prev);

  while (EPS < fabs(cur - prev)) {
    prev = cur;
    cur = prev - f(prev) / f_d(prev);
  }

  return cur;
}

double func(double x) {
  return 4 * x * x * x * x * x + 10 * x * x * x + 4 * x + 90;
}

double func_d(double x) {
  return 20 * x * x * x * x + 30 * x * x + 4;
}

int main() {
  double l = -2, r = -1;

  double root = newton((l + r) / 2.0, func, func_d);

  cout << setprecision(6) << fixed << root << endl;
}
