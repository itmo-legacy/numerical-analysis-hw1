import subprocess
import sys

path_to_executable = "./debug/part2"


def __setup_rust_environment():
    f = None
    has_compiler = False
    try:
        output = subprocess.check_output(["command rustc"], shell=True)
        if len(output) != 0:
            has_compiler = True
    except subprocess.CalledProcessError:
        pass
    if has_compiler:
        print("Found rust compiler")
    else:
        print("There is no rust compiler, please run \n"
              "./install-rust.sh", file=sys.stderr)
        exit(1)
    try:
        f = open(path_to_executable)
    except IOError:
        print("Building rust application")
        subprocess.call(["cargo", "build", "--target-dir", "."])
    finally:
        if f is not None:
            f.close()


__setup_rust_environment()


def __invoke_executable(args: [str]) -> str:
    command = subprocess.Popen([path_to_executable] + args, stdout=subprocess.PIPE)
    return command.communicate()[0].decode("utf-8")


def run_equation(r: float, x0: float) -> float:
    output = __invoke_executable(["run_equation", str(r), str(x0)])
    return float(output)


def build_iteration_sequence(r: float, iterations: int, x0: float) -> [float]:
    output = __invoke_executable(["build_iteration_sequence", str(r), str(iterations), str(x0)])
    return list(map(lambda s: float(s), output.split(" ")))


def evaluate(r: float, iterations: int, x0s: [float]) -> [float]:
    output = __invoke_executable(["evaluate", str(r), str(iterations)] + list(map(lambda x: str(x), x0s)))
    return list(map(lambda s: float(s), output.split(" ")))


# if __name__ == '__main__':
#     print(run_equation(2.5, 0.2))
#     print(build_iteration_sequence(0.228, 100, 0.3))
#     print(evaluate(2.5, 15, [0.01, 0.02, 0.03, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6]))
