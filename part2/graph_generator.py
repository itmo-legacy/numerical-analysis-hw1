import matplotlib.pyplot as plt
import os


def __store(pathname, xs, ys):
    assert len(xs) == len(ys)
    f = open(pathname, "w+")
    for (x, y) in zip(xs, ys):
        f.write(str(x) + " " + str(y) + "\n")
    f.close()


def convergence_theorem(store_data: bool = False):
    rs = [r * 0.0001 for r in range(3000, 40000)]
    lower = list(map(lambda r: 1 / 2 - 1 / (2 * r), rs))
    upper = list(map(lambda r: 1 / 2 + 1 / (2 * r), rs))
    roots = list(map(lambda r: 1 - 1 / r, rs))
    roots2 = list(map(lambda r: 0, rs))
    plt.plot(rs, upper, label="upper convergence bound")
    plt.plot(rs, lower, label="lower convergence bound")
    plt.plot(rs, roots, label="root(1-1/r)")
    plt.plot(rs, roots2, label="root(0)")
    plt.xlabel('r')
    plt.ylabel('convergence range')
    plt.legend()
    plt.show()
    if store_data:
        prefix = "convergence_theorem"
        os.makedirs(prefix, exist_ok=True)
        __store(prefix + "/" + prefix + "_lower", rs, lower)
        __store(prefix + "/" + prefix + "_upper", rs, upper)
        __store(prefix + "/" + prefix + "_first_zero", rs, roots2)
        __store(prefix + "/" + prefix + "_second_zero", rs, roots)


def derivative_sign(r: float, x0: float, scale_left: float = 1, scale_right: float = 1, store_data: bool = False):
    lbound = 1 / 2 - 1 / (2 * r) * scale_left
    rbound = 1 / 2 + 1 / (2 * r) * scale_right
    iters = 1000
    step = (rbound - lbound) / iters
    xs = [lbound + i * step for i in range(0, iters)]
    ys = list(map(lambda x: r * (1 - x) * x, xs))
    ds = list(map(lambda x: x, xs))
    xss = []
    yss = []
    for i in range(10):
        xss.append(x0)
        yss.append(x0)
        xss.append(x0)
        yss.append(r * (1 - x0) * x0)
        x0 = r * (1 - x0) * x0
    plt.plot(xs, ys)
    plt.plot(xs, ds)
    plt.plot(xss, yss)
    plt.show()
    if store_data:
        if r > 2:
            prefix = "negative_derivative"
        else:
            prefix = "positive_derivative"
        os.makedirs(prefix, exist_ok=True)
        __store(prefix + "/" + prefix + "_phi", xs, ys)
        __store(prefix + "/" + prefix + "_straight", xs, ds)
        __store(prefix + "/" + prefix + "_path", xss, yss)


if __name__ == '__main__':
    derivative_sign(1.5, 0.5, scale_left=0.6, scale_right=0.1, store_data=True)
    # derivative_sign(2.5, 0.5, scale_left=0.05, scale_right=0.7, store_data=True)
    # convergence_theorem(store_data=True)
